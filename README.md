# Linear Regression #

# **Updated version can be found here** #
## **https://bitbucket.org/projekt53/machine-learning** ##

_________________________________________________________________________________________
C# implementation of linear regression gradient descent with one variable. Make sure that the folder in which the program is runned at has data.txt with valid data (x,y) 

data example:

* 5,10
* 6,7
* 8.4,6.4
* 9.8,12



## Hypothesis ##


 ![hy.png](https://bitbucket.org/repo/8Lkb4A/images/476186607-hy.png)

## Cost function ##


 ![cost.png](https://bitbucket.org/repo/8Lkb4A/images/2821231500-cost.png)
##  Gradient descent ##


 ![grad.png](https://bitbucket.org/repo/8Lkb4A/images/1114652618-grad.png)

![preview.png](https://bitbucket.org/repo/8Lkb4A/images/1660149359-preview.png)