﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineralReg
{
    class Program
    {
        static void Main(string[] args)
        {
            double alpha = 0;
            int iterations = 0;
            bool succes = false;
            while (!succes)
            {
                try
                {
                    Console.WriteLine("Enter ALPHA");
                    alpha = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter # of iterations");
                    iterations = Convert.ToInt32(Console.ReadLine());
                    succes = true;
                }
                catch
                {
                    Console.WriteLine("Invalid input please retry");
                }
            }
            var data = ReadData();
            var theta = new Theta(0,0);
            Console.WriteLine("Cost function result: "+ Cost(data, theta));
            theta = GradientDescent(data, theta, alpha, iterations);
            Console.WriteLine("Theta 0 :" + theta.Theta0 + " , Theta 1 :" + theta.Theta1);
            while (true)
            {
                try
                {
                    Console.WriteLine("Value X?");
                    double val = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(theta * val);
                }
                catch
                {
                    Console.WriteLine("Invalid input please retry");
                }
            }

        }

        public static Theta GradientDescent(List<DataValue> trainingData, Theta theta,double alpha, int iterations)
        {
            double m = trainingData.Count;
            for (int i = 0; i < iterations; i++)
            {
                double sum0 = 0;
                double sum1 = 0;
                foreach (var dataValue in trainingData)
                {
                    sum0 += (Hypothesis(dataValue.X, theta) - dataValue.Y) * dataValue.X;
                    sum1 += (Hypothesis(dataValue.X, theta) - dataValue.Y);
                }
                theta.Theta0 = theta.Theta0 - (alpha * (1 / m) * sum1);
                theta.Theta1 = theta.Theta1 - (alpha * (1 / m) * sum0);
                Cost(trainingData, theta);
            }
            return theta;
        }

        public static double Cost(List<DataValue> trainingData,Theta theta)
        {
            double f = 2*trainingData.Count;
            double s = 1/f;
            double sum = 0;
            foreach (var data in trainingData)
            {
                sum += Math.Pow(Hypothesis(data.X, theta) - data.Y,2);
            }
            double solution = s*sum;
            return solution;
        }

        public static double Hypothesis(double x, Theta theta)
        {
            return theta.Theta0 + theta.Theta1 * x;
        }

        public static List<DataValue> ReadData()
        {
            using (StreamReader sr = new StreamReader("data.txt"))
            {
                List<DataValue> trainingData = new List<DataValue>();
                while (sr.Peek() >= 0)
                {
                    string str;
                    string[] strArray;
                    str = sr.ReadLine();

                    strArray = str.Split(',');
                    DataValue data = new DataValue();
                    data.X = Convert.ToDouble(strArray[0]);
                    data.Y = Convert.ToDouble(strArray[1]);
                    trainingData.Add(data);
                }
                return trainingData;
            }
        }
    }

    class DataValue
    {
        public double X;
        public double Y;
    }

    class Theta
    {
        public static double operator *(Theta c1, double c2)
        {
            return c1.Theta0 + c1.Theta1 * c2;
        }

        public Theta() { }
        public Theta(double theta0, double theta1)
        {
            Theta0 = theta0;
            Theta1 = theta1;
        }
        public double Theta0 = 0;
        public double Theta1 = 0;
    }
}
